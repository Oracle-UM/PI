/*
 * Description : Imperative Programming 2018-2018 worksheet 5 resolution.
 *               Resolução da ficha 5 de Programação Imperativa 2017-2018.
 * Copyright   : © Oracle, 2018
 * License     : GPL-3.0-or-later
 * Maintainer  : oracle.uminho@gmail.com
 */

// printf
#include <stdio.h>

// 1
void insere(int v[], int N, int x) {
    for (; N > 0 && x < v[N - 1]; --N)
        v[N] = v[N - 1];
    v[N] = x;
}

void iSort(int v[], int N) {
    for (int i = 1; i < N; ++i)
        insere(v, i, v[i]);
}

// 2
void iSort_inline(int v[], int N) {
    for (int i = 1; i < N; ++i) {
        int x = v[i];

        int j;
        for (j = i - 1; j >= 0 && x < v[j]; --j)
            v[j + 1] = v[j];

        v[j + 1] = x;
    }
}

// 3
int maxInd(int v[], int N) {
    int highest_ind = 0;

    for (int i = 1; i < N; ++i) {
        if (v[i] > v[highest_ind])
            highest_ind = i;
    }

    return highest_ind;
}

// 4
void swap(int v[], int i, int j) {
    int temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

void maxIndSort(int v[], int N) {
    for (; N > 0; --N)
        swap(v, N - 1, maxInd(v, N));
}

// 5
void maxIndSort_inline(int v[], int N) {
    for (; N > 0; --N) {
        int highest_ind = 0;

        for (int i = 1; i < N; ++i) {
            if (v[i] > v[highest_ind])
                highest_ind = i;
        }

        swap(v, N - 1, highest_ind);
    }
}

// 6
void bubble(int v[], int N) {
    for (int i = 1; i < N; ++i)
        if (v[i - 1] > v[i])
            swap(v, i - 1, i);
}

static inline void printIntArray(const int v[], int N) {
    for (int i = 0; i < N; ++i)
        printf("%d ", v[i]);
    printf("\n");
}

void bubble_example(void) {
    int v[] = {-1, 1, 2, -5, -2};
    bubble(v, 5);
    printIntArray(v, 5);
}

// 7
void bubbleSort(int v[], int N) {
    for (int i = 0; i < N; ++i)
        bubble(v, N);
}

// 8
int bubble_v2(int v[], int N) {
    int was_altered = 0;

    for (int i = 1; i < N; ++i) {
        if (v[i - 1] > v[i]) {
            swap(v, i - 1, i);
            was_altered = 1;
        }
    }

    return was_altered;
}

void bubbleSort_v2(int v[], int N) {
    for (int i = 0; i < N; ++i)
        if (!bubble_v2(v, N))
            return;
}

// 9
void merge(int r[], int a[], int na, int b[], int nb) {
    int ia = 0, ib = 0, ir = 0;

    while (ia < na && ib < nb)
        r[ir++] = a[ia] < b[ib] ? a[ia++] : b[ib++];

    while (ia < na)
        r[ir++] = a[ia++];

    while (ib < nb)
        r[ir++] = b[ib++];
}

void mergesort(int v[], int n, int aux[]) {
    int m;

    if (n > 1) {
        m = n / 2;

        mergesort(v, m, aux);
        mergesort(v + m, n - m, aux);
        merge(aux, v, m, v + m, n - m);

        for (int i = 0; i < n; ++i)
            v[i] = aux[i];
    }
}

// 10
int partition(int v[], int a, int b) {
    int pivot = v[b];

    for (int i = a; i < b; i++)
        if (v[i] < pivot)
            swap(v, a++, i);
    swap(v, a, b);

    return a;
}

void qsortAux(int v[], int a, int b) {
    if (a < b) {
        int p = partition(v, a, b);

        qsortAux(v, a, p - 1);
        qsortAux(v, p + 1, b);
    }
}

void qsort(int v[], int n) { qsortAux(v, 0, n - 1); }