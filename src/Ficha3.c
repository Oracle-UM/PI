/* Description : Imperative Programming 2018-2018 worksheet 3 resolution.
 *               Resolução da ficha 3 de Programação Imperativa 2017-2018.
 * Copyright   : © Oracle, 2018
 * License     : GPL-3.0-or-later
 * Maintainer  : oracle.uminho@gmail.com
 * The code written in this file doesn't serve as an example of good coding
 * practices, nor do the comments.
 * O código escrito neste ficheiro não serve como exemplo de boas práticas de
 * programação. O mesmo se aplica aos comentários.
 */

// printf
#include <stdio.h>

// 1
// a
void ex1_a(void) {
    int x[15] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    // define a 15 element integer array variable x; assign values to each
    // element;

    int *y, *z, i;  // define 2 integer pointer variables: y, z; define an
                    // integer variable i;
    y = x;      // assign the memory adress of *x to y, in this case the first
                // element of the array (*y = 1)
    z = x + 3;  // assign the memory adress of *(x + 3) to z, in this case the
                // fourth element of the array (*z = 4)
    for (i = 0; i < 5; i++) {  // run the code in the "for" body 5 times
        printf("%d %d %d\n", x[i], *y, *z);  // print the integers in the memory
                                             // addresses stored in x + i, y and
                                             // z to stdout
        y = y + 1;                           // increment y by 1
        z = z + 2;                           // increment z by 2
    }  // at the end of the cycle the following characters will be displayed in
       // stdout
       /*
       "1 1 4"
       "2 2 6"
       "3 3 8"
       "4 4 10"
       "5 5 12
       "
        */
}

// b
int main() {
    int i, j, *a, *b;  // define 2 integer variables: i, j; define two integer
                       // pointer variables: a, b
    i = 3;             // assign the number 3 to i
    j = 5;             // assign the number 5 to j
    a = &i;            // store the address of the variable i in a (*a = i = 3)
    b = &j;            // store the address of the variable j in b (*b = j = 5)
    i++;               // increment i by 1 (4)
    j = i + *b;  // calculate i (4); calculate *b (5); calculate i + *b (4 + 5 =
                 // 9); assign the number 9 to j
    b = a;  // assign the memory address stored in a to b, i.e. b is referencing
            // the same variable as a (*b = *a = 4)
    j = j + *b;  // calculate j (9); calculate *b (4); calculate j + *b (9 + 4 =
                 // 13); assign the number 13 to j
    printf("%d\n", j);  // print j (13) to stdout
    /*
    "13
    "
     */
    return 0;  // return success exit code
}

// 2
void swapM(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

void swapM_example(void) {
    int x = 3, y = 5;
    swapM(&x, &y);
    printf("%d %d\n", x, y);
}

// 3
void swap_v1(int v[], int i, int j) {
    int temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

void swap_v2(int v[], int i, int j) {
    int temp = i[v];
    i[v] = j[v];
    j[v] = temp;
}

void swap_v3(int v[], int i, int j) {
    int temp = *(v + i);
    *(v + i) = *v + j;
    *(v + j) = temp;
}

// 4
int soma_v1(int v[], int N) {
    int sum = 0;

    for (; N > 0; sum += v[--N]);

    return sum;
}

int soma_v2(int v[], int N) {
    int sum = 0;

    for (int i = 0; i < N; sum += v[i++])
        continue;

    return sum;
}

// 5
// EXIT_SUCCESS, EXIT_FAILURE
#include <stdlib.h>

int maximum_v1(int v[], int N, int *m) {
    if (N <= 0)
        return EXIT_FAILURE;

    *m = v[0];

    for (int i = 1; i < N; ++i) {
        if (v[i] > *m)
            *m = v[i];
    }

    return EXIT_SUCCESS;
}

int maximum_v2(int v[], int N, int *m) {
    if (N <= 0)
        return EXIT_FAILURE;

    int max = v[0];

    for (int i = 1; i < N; ++i)
        if (v[i] > max)
            max = v[i];

    *m = max;

    return EXIT_SUCCESS;
}

int maximum_v3(int v[], int N, int *m) {
    if (N <= 0)
        return EXIT_FAILURE;

    int max = v[--N];

    for (--N; N >= 0; --N)
        if (v[N] > max)
            max = v[N];

    *m = max;

    return EXIT_SUCCESS;
}

// 6
void quadrados(int q[], int N) {
    for (int i = 1; i <= N; ++i)
        q[i - 1] = i * i;
}

// 7
void pascal(int v[], int N) {
    v[0] = 1;
    
    for (int t = 1; t < N; ++t) {
        v[t] = 1;
        for (int i = t - 1; i > 0; --i)
            v[i] = v[i] + v[i - 1];
    }
}
