#include <stdio.h>

void putnchar(int n, char c);

void triangulo(int n) {
    for (int i = 1; i < n; ++i) {
        putnchar(i, '#');
        putchar('\n');
    }
    for (int i = n; i > 0; --i) {
        putnchar(i, '#');
        putchar('\n');
    }
}
