#include <stdio.h>

void putnchar(int n, char c);

void triangulo(int n) {
    for (int i = 0; i < n; ++i) {
        putnchar(n - i - 1, ' ');
        putnchar(2 * i + 1, '#');
        putchar('\n');
    }
}
