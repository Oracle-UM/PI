#include <stdio.h>

void putnchar(int n, char c) {
    while (n--)
        putchar(c);
}
