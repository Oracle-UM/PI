/*
 * Description : Imperative Programming 2018-2018 worksheet 1 resolution.
 *               Resolução da ficha 1 de Programação Imperativa 2017-2018.
 * Copyright   : © Oracle, 2018
 * License     : GPL-3.0-or-later
 * Maintainer  : oracle.uminho@gmail.com
 * The code written in this file doesn't serve as an example of good coding
 * practices, nor do the comments.
 * O código escrito neste ficheiro não serve como exemplo de boas práticas de
 * programação. O mesmo se aplica aos comentários.
 */

// printf, putchar
#include <stdio.h>

// Estado e atribuições
// 1
// Define the function ex1_1.
void ex1_1(void) {
    int x, y;   // define two integer variables: x, y
    x = 3;      // assign the number 3 to x
    y = x + 1;  // calculate x (3); calculate x + 1 (3 + 1 = 4); assign the
                // number 4 to y
    x = x * y;  // calculate x (3); calculate y (4); calculate x * y (3 * 4 =
                // 12); assign the number 12 to x
    y = x + y;  // calculate x (12); calculate y (4); calculate x + y (12 + 4 =
                // 16); assign the number 16 to y
    printf("%d %d\n", x, y);  // print x (12) (space) and y (16) (and newline)
                              // to stdout
    /*
     "12 16
     "
     */
}

// 2
// Define the function ex1_2.
void ex1_2(void) {
    int x, y;                 // define two integer variables: x, y
    x = 0;                    // assign the number 3 to x
    printf("%d %d\n", x, y);  // print x (0) (space) and y (uninitialized)
                              // (and newline) to stdout
    /*
    "0 ind
    "
    where ind is an indeterminate value caused by undeﬁned behavior (accessing
    an uninitialized variable)
     */
}

// 3
// Define the function ex1_3.
void ex1_3(void) {
    char a, b, c;             // define three character variables: a, b, c
    a = 'A';                  // assign the character 'A' to a
    b = ' ';                  // assign the character ' ' to b
    c = '0';                  // assign the character '0' to c
    printf("%c %d\n", a, a);  // print a ('A') (space) and the ASCII value
                              // associated with the character 'A' (65) (and
                              // newline) to stdout
    /*
    "A 65
    "
     */
    a = a + 1;  // calculate a ('A'); calculate a + 1 (65 + 1 = 66); assign the
                // character associated with the ASCII value 66 ('B') to a
    c = c + 2;  // calculate c ('0'); calculate c + 2 (48 + 2 = 50); assign the
                // character associated with the ASCII value 50 ('2') to c
    printf("%c %d %c %d\n", a, a, c, c);  // print a ('B'), (space) the ASCII
                                          // value associated with the character
                                          // 'B' (66), (space) c ('2') (space)
                                          // and the ASCII value associated with
                                          // the character '2' (50) (and
                                          // newline) to stdout
    /*
    "B 66 2 50
    "
     */
    c = a + b;  // calculate a ('B'); calculate b (' '); calculate a + b (66 +
                // 32 = 98); assign the character associated with the ASCII
                // value 98 ('b') to c
    printf("%c %d\n", c, c);  // print c ('b') (space) and the ASCII value
                              // associated with the character 'b' (98) (and
                              //  newline) to stdout
    /*
    "b 98
    "
     */
}

// 4
// Define the function ex1_4.
void ex1_4(void) {
    int x, y;   // define two integer variables: x, y
    x = 200;    // assign the number 200 to x
    y = 100;    // assign the number 100 to y
    x = x + y;  // calculate x (200); calculate y (100); calculate x + y (200 +
                // 100 = 300); assign the number 300 to x
    y = x - y;  // calculate x (300); calculate y (100); calculate x - y (300 -
                // 100 = 200); assign the number 200 to y
    x = x - y;  // calculate x (300); calculate y (200); calculate x - y (300 -
                // 200 = 100); assign the number 100 to x
    printf("%d %d\n", x, y);  // print x (100) and (space) y (200) (and newline)
                              // to stdout
    /*
    "100 200
    "
     */
}

// 5
// Define the function ex1_5.
void ex1_5(void) {
    int x, y;  // define two integer variables: x, y
    x = 100;   // assign the number 100 to x
    y = 28;    // assign the number 28 to y
    x += y;  // calculate x (100); calculate y (28); calculate x + y (100 + 28 =
             // 128); assign the number 128 to x
    y -= x;  // calculate y (28); calculate x (128); calculate y - x (28 - 128 =
             // -100); assign the number -100 to y
    printf("%d %d\n", x++, ++y);  // calculate x (128); calculate y + 1
                                  // (-100 + 1 = -99); assign the number -99 to
                                  // y; print x (128) (space) and y (-99) (and
                                  // newline) to stdout; calculate x + 1 (128 +
                                  // 1 = 129); assign the number 129 to x;
    /*
    "128 -99
    "
     */
    printf("%d %d\n ", x, y);  // print x (129) (space) and y (-99) (and
                               // newline) to stdout
    /*
    "129 -99
    "
    */
}

// Estruturas de controlo
// 1
// a
// Define the function ex2_1_a.
void ex2_1_a(void) {
    int x, y;                 // define two integer variables: x, y
    x = 3;                    // assign the number 3 to x
    y = 5;                    // assign the number 5 to y
    if (x > y)                // check if x > y (3 > 5 -> evaluates to 0)
        y = 6;                // skipped from execution
    printf("%d %d\n", x, y);  // prints x (3) (space) and y (5) (and newline)
                              // to stdout
    /*
    "3 5
    "
     */
}

// b
// Define the function ex2_1_b.
void ex2_1_b(void) {
    int x, y;   // define two integer variables: x, y
    x = y = 0;  // assign the number 0 to y; calculate y (0); assign the number
                // 0 to x
    while (x != 11) {  // check if x isn't 11; while the condition evaluates to
                       // 1, execute the body of the "while"
        x = x + 1;     // increment x by 1
        y += x;  // calculate y; calculate x; calculate y + x; assign the number
                 // y + x to y
    }            // at the end of the cycle, x = 11 and y = 66
    printf("%d %d\n", x, y);  // print x (11) (space) and y (66) (and newline)
                              // to stdout
    /*
    "11 66
    "
     */
}

// c
// Define the function ex2_1_c.
void ex2_1_c(void) {
    int x, y;   // define two integer variables: x, y
    x = y = 0;  // assign the number 0 to y; calculate y (0); assign the number
                // 0 to x
    while (x != 11) {  // check if x isn't 11; while the condition evaluates to
                       // 1, execute the body of the "while"
        x = x + 2;     // calculate x; calculate x + 2; assign the number x + 2
                       // to x
        y += x;  // calculate y; calculate x; calculate y + x; assign the number
                 // y + x to y
    }  // Since x will always be even, the condition wil always be satisfied,
       // which will cause an endless loop
    printf("%d %d\n", x, y);  // skipped from execution
}

// d
// Define the function ex2_1_d.
void ex2_1_d(void) {
    int i;                      // define an integer variable i
    for (i = 0; (i < 20); i++)  // assign the number 0 to i; check if i is less
                                // than 20; while the condition evaluates to 1,
                                // execute the body of the "while", then
                                // increment i by 1
        if (i % 2 == 0)         // check if i is even
            putchar('_');  // if the condition evaluates to nonzero, print the
                           // character '_' to stdout
        else
            putchar('#');  // if the condition evaluates to zero, print the
                           // character '#' to stdout at the end of the cycle
                           // the characters "_#_#_#_#_#_#_#_#_#_#" will be
                           // desplayed in stdout
}

// e
// Define the function ex2_1_e.
void ex2_1_e(void) {
    char i, j;                        // define two character variables: i, j
    for (i = 'a'; (i != 'h'); i++) {  // assign the character 'a' to i; check
                                      // if i isn't 'h'; while the condition
                                      // evaluates to 1, execute the body of the
                                      // "while", then increment i by 1
        for (j = i; (j != 'h'); j++)  // assign the character stored in i to j;
                                      // check if j isn't 'h'; while the
                                      // condition evaluates to 1, execute the
                                      // body of the "while", then increment j
                                      // by 1
            putchar(j);               // print j to stdout
        putchar('\n');                // print newline to stdout
    }  // at the end of the cycle the following characters will be displayed in
       // stdout:
       /*
       "abcdefg
        bcdefg
        cdefg
        defg
        efg
        fg
        g
        "
        */
}

// f
// Define the function f.
void f(int n) {
    while (n > 0) {        // check if n is greater than 0; while the condition
                           // evaluates to 1, execute the body of the "while"
        if (n % 2 == 0)    // check if n is even
            putchar('0');  // if the condition evaluates to nonzero, print the
                           // character '0' to stdout
        else
            putchar('1');  // if the condition evaluates to zero, print the
                           // character '1' to stdout
        n = n / 2;         // halve n
    }
    putchar('\n');  // print newline to stdout
}

// Define the function main.
int main() {
    int i;                      // define an integer variable i
    for (i = 0; (i < 16); i++)  // run the code in the "for" body 16 times
        f(i);                   // call the function f with the argument i;
    // at the end of the cycle the following characters will be displayed in
    // stdout:
    /*
    "
    1
    01
    11
    001
    101
    011
    111
    0001
    1001
    0101
    1101
    0011
    1011
    0111
    1111
    "
     */
    return 0;  // return success exit code
}

// 2
void putnchar(int n, char c) {
    while (n--)
        putchar(c);
}

void quadrado(int n) {
    for (int i = 0; i < n; ++i) {
        putnchar(n, '#');
        putchar('\n');
    }
}

// 3
void xadrez(int n) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j)
            putchar(((i + j) % 2 == 0) ? '#' : '_');
        putchar('\n');
    }
}
