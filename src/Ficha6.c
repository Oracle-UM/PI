/*
 * Description : Imperative Programming 2018-2018 worksheet 6 resolution.
 *               Resolução da ficha 6 de Programação Imperativa 2017-2018.
 * Copyright   : © Oracle, 2018
 * License     : GPL-3.0-or-later
 * Maintainer  : oracle.uminho@gmail.com
 */

// fprintf
#include <stdio.h>

// malloc, realloc, free
#include <stdlib.h>

// memcpy
#include <string.h>

#define MAX 100  // static array size

// 1
typedef struct StaticStack {
    int sp;  // stores the last used index
    int valores[MAX];
} StaticStack;

// a
void init_StaticStack(StaticStack *s) { s->sp = -1; }

// b
int isEmpty_StaticStack(StaticStack *s) { return -1 == s->sp; }

// c
static inline int isFull_StaticStack(const StaticStack *const s) {
    return MAX - 1 == s->sp;
}

int push_StaticStack(StaticStack *s, int x) {
    if (isFull_StaticStack(s))
        return 1;

    s->valores[++s->sp] = x;

    return 0;
}

// d
int pop_StaticStack(StaticStack *s, int *x) {
    if (isEmpty_StaticStack(s))
        return 1;

    *x = s->valores[s->sp--];

    return 0;
}

// e
int top_StaticStack(StaticStack *s, int *x) {
    if (isEmpty_StaticStack(s))
        return 1;

    *x = s->valores[s->sp];

    return 0;
}

// 2
typedef struct StaticQueue {
    int inicio, tamanho;
    int valores[MAX];
} StaticQueue;

// a
void init_StaticQueue(StaticQueue *q) { q->inicio = q->tamanho = 0; }

// b
int isEmpty_StaticQueue(StaticQueue *q) { return 0 == q->tamanho; }

// c
static inline int isFull_StaticQueue(StaticQueue *q) {
    return MAX == q->tamanho;
}

int enqueue_StaticQueue(StaticQueue *q, int x) {
    if (isFull_StaticQueue(q))
        return 1;

    q->valores[(q->inicio + q->tamanho++) % MAX] = x;

    return 0;
}

// d
int dequeue_StaticQueue(StaticQueue *q, int *x) {
    if (isEmpty_StaticQueue(q))
        return 1;

    *x = q->valores[q->inicio];
    q->inicio = (q->inicio + 1) % MAX;
    --q->tamanho;

    return 0;
}

// e
int front_StaticQueue(StaticQueue *q, int *x) {
    if (isEmpty_StaticQueue(q))
        return 1;

    *x = q->valores[q->inicio];

    return 0;
}

// 3
// a
#define START_ARRAY_SIZE 10

#define ALLOC_MULTIPLIER 2

typedef struct DynamicStack {
    int size;
    int sp;
    int *valores;
} DynamicStack;

void init_DynamicStack(DynamicStack *s) {
    s->sp = -1;
    s->valores = malloc(START_ARRAY_SIZE * sizeof(int));
    s->size = START_ARRAY_SIZE;
}

int isEmpty_DynamicStack(DynamicStack *s) { return -1 == s->sp; }

static inline int isFull_DynamicStack(const DynamicStack *const s) {
    return s->size - 1 == s->sp;
}

int push_DynamicStack_v1(DynamicStack *s, int x) {
    if (isFull_DynamicStack(s)) {
        int new_size = ALLOC_MULTIPLIER * s->size;
        int *new_valores = NULL;

        if (NULL == (new_valores = malloc(new_size * sizeof(int)))) {
            fprintf(stderr, "Out of memory\n");
            return 1;
        }

        memcpy(new_valores, s->valores, new_size * sizeof(int));

        free(s->valores);

        s->valores = new_valores;
        s->size = new_size;
    }

    s->valores[++s->sp] = x;

    return 0;
}

// in-place
int push_DynamicStack_v2(DynamicStack *s, int x) {
    if (isFull_DynamicStack(s)) {
        int new_size = ALLOC_MULTIPLIER * s->size;

        if (NULL == (s->valores = realloc(s->valores, new_size * sizeof(int)))) {
            fprintf(stderr, "Out of memory\n");
            return 1;
        }

        s->size = new_size;
    }

    s->valores[++s->sp] = x;

    return 0;
}

int pop_DynamicStack(DynamicStack *s, int *x) {
    if (isEmpty_DynamicStack(s))
        return 1;

    *x = s->valores[s->sp--];

    return 0;
}

int top_DynamicStack(DynamicStack *s, int *x) {
    if (isEmpty_DynamicStack(s))
        return 1;

    *x = s->valores[s->sp];
    return 0;
}

// b
typedef struct DynamicQueue {
    int inicio;
    int tamanho;  // number of used array elements
    int size;     // number of array elements
    int *valores;
} DynamicQueue;

void init_DynamicQueue(DynamicQueue *q) {
    q->inicio = q->tamanho = 0;
    q->valores = malloc(START_ARRAY_SIZE * sizeof(int));
    q->size = START_ARRAY_SIZE;
}

int isEmpty_DynamicQueue(DynamicQueue *q) { return 0 == q->tamanho; }

static inline int isFull_DynamicQueue(const DynamicQueue *const q) {
    return q->tamanho == q->size;
}

int enqueue_DynamicQueue_v1(DynamicQueue *q, int x) {
    if (isFull_DynamicQueue(q)) {
        int new_size = ALLOC_MULTIPLIER * q->size;
        int *new_valores = NULL;

        if (NULL == (new_valores = malloc(new_size * sizeof(int)))) {
            fprintf(stderr, "Out of memory\n");
            return 1;
        }

        memcpy(new_valores, q->valores + q->inicio,
               (q->tamanho - q->inicio) * sizeof(int));
        memcpy(new_valores + q->tamanho - q->inicio, q->valores,
               q->inicio * sizeof(int));

        free(q->valores);

        q->valores = new_valores;
        q->size = new_size;
        q->inicio = 0;
    }

    q->valores[(q->tamanho++ + q->inicio) % q->size] = x;

    return 0;
}

// in-place
int enqueue_DynamicQueue_v2(DynamicQueue *q, int x) {
    if (isFull_DynamicQueue(q)) {
        int new_size = ALLOC_MULTIPLIER * q->size;

        if (NULL == (q->valores = realloc(q->valores, new_size * sizeof(int)))) {
            fprintf(stderr, "Out of memory\n");
            return 1;
        }

        if (q->inicio != 0) {
            int new_inicio = q->inicio + q->tamanho;
            memcpy(q->valores + new_inicio, q->valores + q->inicio,
                   (q->tamanho - q->inicio) * sizeof(int));
            q->inicio = new_inicio;
        }

        q->size = new_size;
    }

    q->valores[(q->tamanho++ + q->inicio) % q->size] = x;

    return 0;
}

int dequeue_Dynamic_Queue(DynamicQueue *q, int *x) {
    if (isEmpty_DynamicQueue(q))
        return 1;

    *x = q->valores[q->inicio];
    q->inicio = (q->inicio + 1) % q->size;
    --q->tamanho;

    return 0;
}

int front_DynamicQueue(DynamicQueue *q, int *x) {
    if (isEmpty_DynamicQueue(q))
        return 1;

    *x = q->valores[q->inicio];

    return 0;
}
