/*
 * Description : Imperative Programming 2018-2018 worksheet 4 resolution.
 *               Resolução da ficha 4 de Programação Imperativa 2017-2018.
 * Copyright   : © Oracle, 2018
 * License     : GPL-3.0-or-later
 * Maintainer  : oracle.uminho@gmail.com
 */

// isspace
#include <ctype.h>

// memset
#include <string.h>

// 1
#define IS_UPPER(c) ((c) >= 'A' && (c) <= 'Z')

#define UPPER_TO_LOWER(c) ((c) += 'a' - 'A')

int minusculas_v1(char s[]) {
    int lower_count = 0;

    for (int i = 0; s[i] != '\0'; ++i)
        if (IS_UPPER(s[i])) {
            UPPER_TO_LOWER(s[i]);
            ++lower_count;
        }

    return lower_count;
}

int minusculas_v2(char *s) {
    int lower_count = 0;

    for (; *s; ++s)
        if (IS_UPPER(*s)) {
            UPPER_TO_LOWER(*s);
            ++lower_count;
        }

    return lower_count;
}

// 2
int contalinhas_v1(char s[]) {
    int line_count = 1;

    for (int i = 0; s[i] != '\0'; ++i)
        if (s[i] == '\n')
            ++line_count;

    return line_count;
}

int contalinhas_v2(char s[]) {
    int line_count = 1;

    for (; *s; ++s)
        if (*s == '\n')
            ++line_count;

    return line_count;
}

// 3
int contaPal(char s[]) {
    int word_count = 0;

    for (int i = 0; s[i]; ++i)
        if (!isspace(s[i]) && (i == 0 || isspace(s[i - 1])))
            ++word_count;

    return word_count;
}

// 4
int mystrcmp(const char *s1, const char *s2) {
    for (; *s1 && *s1 == *s2; ++s1, ++s2)
        continue;

    return (*s1 - *s2);
}

int procura(char *p, char *ps[], int N) {
    for (int i = 0; i < N; ++i)
        if (strcmp(p, ps[i]) == 0)
            return i;

    return -1;
}

// 5
int procuraOrd(const char *p, const char *ps[], int N) {
    int i;
    for (i = 0; i < N && mystrcmp(p, ps[i]) > 0; ++i)
        continue;
    return i < N ? i : -1;
}

// 6
int pBinRAux(const char *p, const char *ps[], int l, int r) {
    if (l > r)
        return -1;

    int mid = (l + r) / 2;
    int cmp = mystrcmp(p, ps[mid]);

    if (cmp < 0)
        return pBinRAux(p, ps, l, mid - 1);
    else if (cmp > 0)
        return pBinRAux(p, ps, mid + 1, r);
    else
        return mid;
}

int pBinR(const char *p, const char *ps[], int N) {
    return pBinRAux(p, ps, 0, N - 1);
}

int pBinIt(const char *p, const char *ps[], int N) {
    int l = 0;
    int r = N - 1;
    while (l <= r) {
        int mid = (l + r) / 2;
        int cmp = mystrcmp(p, ps[mid]);

        if (cmp < 0)
            r = mid - 1;
        if (cmp > 0)
            l = mid + 1;
        if (cmp == 0)
            return mid;
    }

    return -1;
}

// 7
int zeros(int N, int M, int m[N][M]) {
    int zero_count = 0;

    for (int row = 0; row < N; ++row)
        for (int col = 0; col < M; ++col)
            if (m[row][col] == 0)
                ++zero_count;

    return zero_count;
}

// 8
void identidade_v1(int N, int m[N][N]) {
    for (int row = 0; row < N; ++row)
        for (int col = 0; col < N; ++col)
            m[row][col] = row == col;
}

void identidade_v2(int N, int m[N][N]) {
    memset(m, 0, N * N * sizeof(int));
    for (int i = 0; i < N; ++i)
        m[i][i] = 1;
}

// 9
void multV(int N, float t[N][N], float v[N], float r[N]) {
    for (int row = 0; row < N; ++row) {
        float accum = 0;

        for (int col = 0; col < N; ++col)
            accum += t[row][col] * v[col];

        r[row] = accum;
    }
}
