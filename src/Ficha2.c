/*
 * Description : Imperative Programming 2018-2018 worksheet 2 resolution.
 *               Resolução da ficha 2 de Programação Imperativa 2017-2018.
 * Copyright   : © Oracle, 2018
 * License     : GPL-3.0-or-later
 * Maintainer  : oracle.uminho@gmail.com
 */

// 1
float multInt_v1(int n, float m) {
    float r = 0;

    for (; n > 0; --n)
        r += m;
    for (; n < 0; ++n)
        r -= m;

    return r;
}

// 2
float multInt_v2(int n, float m) {
    float r = 0;

    for (; n != 0; m *= 2, n /= 2)
        if (n % 2 != 0)
            r += (n > 0) ? m : -m;

    return r;
}

// 3
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

int mdc(int a, int b) {
    int r = MIN(a, b);

    while (r > 0 && (a % r != 0 || b % r != 0))
        --r;

    return r;
}

// 4
int mdc_v1(int a, int b) {
    while (a != 0 && b != 0)
        a > b ? (a -= b) : (b -= a);

    return a + b;
}

// 5
int mdc_v2(int a, int b) {
    while (b != 0) {
        int c = a % b;
        a = b;
        b = c;
    }

    return a;
}

// 6
// a
int fib_v1(int n) { return (n < 2) ? 1 : fib_v1(n - 1) + fib_v1(n - 2); }

// b
int fib_v2(int n) {
    int t0 = 1, t1 = 1, tnext;

    for (; n > 0; --n) {
        tnext = t0 + t1;
        t0 = t1;
        t1 = tnext;
    }
    
    return t1;
}
