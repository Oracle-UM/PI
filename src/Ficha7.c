/*
 * Description : Imperative Programming 2018-2018 worksheet 7 resolution.
 *               Resolução da ficha 7 de Programação Imperativa 2017-2018.
 * Copyright   : © Oracle, 2018
 * License     : GPL-3.0-or-later
 * Maintainer  : oracle.uminho@gmail.com
 */

// malloc, free
#include <stdlib.h>

// 1
typedef struct slist *LInt;

typedef struct slist {
    int valor;
    LInt prox;
} Nodo;

// a

LInt ex1_a_v1(void) {
    LInt a2 = malloc(sizeof(Nodo));
    a2->valor = 15;
    a2->prox = NULL;

    LInt a1 = malloc(sizeof(Nodo));
    a1->valor = 5;
    a1->prox = a2;

    LInt a = malloc(sizeof(Nodo));
    a->valor = 10;
    a->prox = a1;

    return a;
}

void ex1_a_v2(LInt a) {
    LInt a2 = malloc(sizeof(Nodo));
    a2->valor = 15;
    a2->prox = NULL;

    LInt a1 = malloc(sizeof(Nodo));
    a1->valor = 5;
    a1->prox = a2;

    a->valor = 10;
    a->prox = a1;
}

// b
// i
LInt cons(LInt l, int x) {
    LInt head = malloc(sizeof(Nodo));
    head->valor = x;
    head->prox = l;
    return head;
}

// ii
LInt tail(LInt l) {
    LInt tail = l->prox;
    free(l);
    return tail;
}

// iii
LInt init(LInt l) {
    LInt init = l;
    LInt prev = NULL;

    while (l->prox) {
        prev = l;
        l = l->prox;
    }

    if (!prev)
        init = NULL;
    else
        prev->prox = NULL;

    free(l);
    return init;
}

void init_recursive_aux(LInt l) {
    if (!l->prox->prox) {
        free(l->prox);
        l->prox = NULL;
    } else
        init_recursive_aux(l->prox);
}

LInt init_recursive(LInt l) {
    if (!l->prox) {
        free(l);
        l = NULL;
    } else
        init_recursive_aux(l);

    return l;
}

// iv
LInt snoc(LInt l, int x) {
    LInt new = cons(NULL, x);

    if (!l)
        return new;

    LInt traverse = l;
    while (traverse->prox)
        traverse = traverse->prox;
    traverse->prox = new;

    return l;
}

void snoc_recursive_aux(LInt l, int x) {
    if (!l->prox)
        l->prox = cons(NULL, x);
    else
        snoc_recursive_aux(l->prox, x);
}

LInt snoc_recursive(LInt l, int x) {
    if (!l)
        l = cons(NULL, x);
    else
        snoc_recursive_aux(l, x);

    return l;
}

// v
LInt concat(LInt a, LInt b) {
    if (!a)
        return b;
    if (!b)
        return a;

    LInt head = a;
    while (a->prox)
        a = a->prox;
    a->prox = b;

    return head;
}

void concat_recursive_aux(LInt a, LInt b) {
    if (!a->prox)
        a->prox = b;
    else
        concat_recursive_aux(a->prox, b);
}

LInt concat_recursive(LInt a, LInt b) {
    if (!a)
        return b;
    if (!b)
        return a;

    concat_recursive_aux(a, b);

    return a;
}

// 2
// a
typedef struct aluno {
    char nome[60];
    int numero;
    float nota;
} Aluno;

typedef struct turma {
    Aluno aluno;
    struct turma *prox;
} * Turma;

// b
int acrescentaAluno(Turma *t, Aluno a) {
    Turma traverse = *t;
    Turma prev = NULL;

    while (traverse && traverse->aluno.numero < a.numero) {
        prev = traverse;
        traverse = traverse->prox;
    }

    if (traverse && a.numero == traverse->aluno.numero)
        return 1;

    Turma new = malloc(sizeof(struct turma));
    new->aluno = a;
    new->prox = traverse;

    if (!prev)
        *t = new;
    else
        prev->prox = new;

    return 0;
}

int acrescentaAluno_v2(Turma *t, Aluno a) {
    while (*t && (*t)->aluno.numero < a.numero)
        t = &((*t)->prox);

    if (*t && a.numero == (*t)->aluno.numero)
        return 1;

    Turma new = malloc(sizeof(struct turma));
    new->aluno = a;
    new->prox = *t;

    *t = new;

    return 0;
}

// c
Aluno *procura(Turma t, int numero) {
    for (; t && t->aluno.numero != numero; t = t->prox)
        continue;

    return t ? &(t->aluno) : NULL;
}

// d
int alunosAprovados(Turma t) {
    int alunos_aprovados = 0;

    for (; t; t = t->prox)
        if (t->aluno.nota >= 10)
            ++alunos_aprovados;

    return alunos_aprovados;
}
