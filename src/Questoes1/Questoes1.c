#include <stdio.h>

// 1
void fstMax(void)
{
	int i, read, fst;
	scanf("%d", &read);
	fst = read;
	for (i = 0; read; ++i)
	{
		if (read > fst)
			fst = read;
		scanf("%d", &read);
	}
	if (i < 1)
		puts("NaN");
	else
		printf("%d\n", fst);
}

// 2
void average(void)
{
	int i;
	float read, summ;
	scanf("%f", &read);
	summ = read;
	for (i = 0; read; ++i)
	{
		scanf("%f", &read);
		summ += read;
	}
	if (i < 1)
		puts("NaN");
	else
		printf("%f\n", summ / i);
}

// 3
void sndMax(void)
{
	int i, read, snd, fst;
	scanf("%d", &read);
	fst = snd = read;
	for (i = 0; read; ++i)
	{
		if (read > fst)
		{
			snd = fst;
			fst = read;
		}
		else
			if (read > snd || snd == fst)
				snd = read;
		scanf("%d", &read);
	}
	if (i < 2)
		puts("NaN");
	else
		printf("%d\n", snd);
}

// 4
int bitsUm(unsigned int n)
{
	int modsumm = 0;
	while (n)
	{
		modsumm += n % 2;
		n /= 2;
	}
	return modsumm;
}

// 5
int trailingZ(unsigned int n)
{
	int i;
	for (i = 0; n % 2 == 0; ++i)
		n /= 2;
	return i;
}

// 6
int qDig(unsigned int n)
{
	int i;
	for (i = 0; n; ++i)
		n /= 10;
	return i;
}

// 7
char *mystrcat(char s1[], char s2[])
{
	char *s0 = s1;
	while (*s1)
		++s1;
	while (*s2)
		*s1++ = *s2++;
	*s1 = '\0';
	return s0;
}

// 8
char *mystrcpy(char *dest, char source[])
{
	char *dest0 = dest;
	while (*source)
		*dest++ = *source++;
	*dest = '\0';
	return dest0;
}

// 9
int mystrcmp(char s1[], char s2[])
{
	while (*s1 && *s1 == *s2)
	{
		++s1;
		++s2;
	}
	return *s1 - *s2;
}

// 10
int startsWith(char s1[], char s2[])
{
	while (*s2 && *s1 == *s2)
	{
		++s1;
		++s2;
	}
	return *s2 == '\0';
}

char *mystrstr(char s1[], char s2[])
{
	for ( ; *s1; ++s1)
		if (startsWith(s1, s2))
			return s1;
	return NULL;
}

// 11
void swap(char *x, char *y)
{
	char temp;
	temp = *x;
	*x = *y;
	*y = temp;
}

int mystrlen(char s[])
{
	int i = 0;
	for ( ; s[i]; ++i);
	return i;
}

void mystrrev(char s[])
{
	char *t = s + mystrlen(s)-1;
	while (s < t)
	{
		swap(s, t);
		++s;
		--t;
	}
}

// 12
char vowels[] = "aeiouAEIOU";

int isVowel(char c)
{
	for (char *p = vowels; *p; ++p)
		if (c == *p)
			return 1;
	return 0;
}

void strnoV(char s[])
{
	char *dest = s;
	while (*s)
		if (isVowel(*s))
			++s;
		else
			*dest++ = *s++;
	*dest = '\0';
}

// 13
void truncW(char t[], int n)
{
	char *dest = t;
	int len = 0;
	while (*t)
	{
		if (*t == ' ')
		{
			*dest++ = *t++;
			len = 0;
		}
		else
			if (len < n)
			{
				*dest++ = *t++;
				++len;
			}
			else
				++t;
	}
	*dest = '\0';
}

// 14
char charMaisfreq(char s[])
{
	int array[256] = {0};
	int c, max = 0;
	for (int i = 0; s[i]; ++i)
	{
		c = s[i];
		++array[c];
		if (array[c] > array[max])
			max = c;
	}
	return max;
}

// 15
int iguaisConsecutivos(char s[])
{
	int r = 1, best = 1;
	if (!*s)
		return 0;
	for (int i = 1; s[i]; ++i)
		if (s[i] == s[i-1])
		{
			++best;
			if (best > r)
				r = best;
		}
		else
			best = 1;
	return r;
}

// 16
int hasChar(char s[], char *fim, char c)
{
	for ( ; s < fim; ++s)
		if (c == *s)
			return 1;
	return 0;
}

int difConsecutivos(char s[])
{
	int best = 0;
	char *fim;
	for ( ; *s; ++s)
	{
		for (fim = s + 1; *fim && !hasChar(s, fim, *fim); ++fim);
		if (fim - s > best)
			best = fim - s;
	}
	return best;
}

// 17
int maiorPrefixo(char s1[], char s2[])
{
	int i;
	for (i = 0; s1[i] && s1[i] == s2[i]; ++i);
	return i;
}

// 18
int maiorSufixo(char s1[], char s2[])
{
	int i1 = mystrlen(s1) - 1, i2 = mystrlen(s2) - 1, r = 0;
	while (i1 >= 0 && i2 >= 0 && s1[i1] == s2[i2])
	{
		--i1;
		--i2;
		++r;
	}
	return r;
}

// 19
int sufPref(char s1[], char s2[])
{
	for (char *s = s1; *s; ++s)
		if (startsWith(s2, s))
			return mystrlen(s);
	return 0;
}

// 20
int contaPal(char s[])
{
	int count = 0;
	for (int i = 0; s[i]; ++i)
		if (s[i] != ' ' && (i == 0 || s[i-1] == ' '))
			++count;
	return count;
}

// 21
int contaVogais(char s[])
{
	int count = 0;
	for (int i = 0; s[i]; ++i)
		if (isVowel(s[i]))
			++count;
	return count;
}

// 22
int contida(char a[], char b[])
{
	for (int i = 0; a[i]; ++i)
		if (!hasChar(b, b + mystrlen(b), a[i]))
			return 0;
	return 1;
}

// 23
int palindorome(char s[])
{
	int i = 0, j = mystrlen(s) - 1;
	while (i < j)
	{
		if (s[i] != s[j])
			return 0;
		++i;
		--j;
	}
	return 1;
}

// 24
int remRep(char s[])
{
	int count = 0;
	for (int i = 0; s[i]; ++i)
		if (s[i] != s[i-1] || i == 0)
			++count;
	return count;
}

// 25
int limpaEspacos(char t[])
{
	int max = mystrlen(t);
	for (int i = 1; t[i]; ++i)
		if (t[i] == ' ' && t[i-1] == ' ')
			--max;
	return max;
}

// 26
void insere(int v[], int N, int x)
{
	for ( ; N > 0 && x < v[N-1]; --N)
		v[N] = v[N - 1];
	v[N] = x;
}

// 27
void merge(int r [], int a[], int b[], int na, int nb)
{
	int ia = 0, ib = 0, ir = 0;
	while (ia < na && ib < nb)
		if (a[ia] < b[ib])
			r[ir++] = a[ia++];
		else
			r[ir++] = b[ib++];
	while (ia < na)
		r[ir++] = a[ia++];
	while (ib < nb)
		r[ir++] = b[ib++];
}

// 28
int crescente(int a[], int i, int j)
{
	for ( ; i < j; ++i)
		if (a[i] > a[i+1])
			return 0;
	return 1;
}

// 29
int retiraNeg(int v[], int N)
{
	int max = N;
	for (int i = 0; i < N; ++i)
		if (v[i] < 0)
			--max;
	return max;
}

// 30
int menosFreq(int v[], int N)
{
	int best = v[0], bestCount = N, lastCount = 1;
	for (int i = 1; i < N; ++i)
	{
		if (v[i] == v[i - 1])
			++lastCount;
		else
		{
			if (lastCount < bestCount)
			{
				bestCount = lastCount;
				best = v[i - 1];
			}
			lastCount = 1;
		}
	}
	if (lastCount < bestCount)
		best = v[N - 1];
	return best;
}

// 31
int maisFreq(int v[], int N)
{
	int best = v[0], countbest = 0, lastCount = 1;
	for (int i = 1; i < N; ++i)
	{
		if (v[i] == v[i - 1])
			++lastCount;
		else
		{
			if (lastCount > countbest)
			{
				best = v[i - 1];
				countbest = lastCount;
			}
			lastCount = 1;
		}
	}
	if (lastCount > countbest)
		best = v[N - 1];
	return best;
}

// 32
int maxCresc(int v[], int N)
{
	int lastcount = 1, best = 1;
	for (int i = 0, j = 1; j < N; ++i, ++j)
	{
		if (crescente(v, i, j))
		{
			++lastcount;
			if (lastcount > best)
				best = lastcount;
		}
		else
			lastcount = 1;
	}
	return best;
}

// 33
int pertence(int v[], int N, int x)
{
	for (int i = 0; i < N; ++i)
		if (x == v[i])
			return 1;
	return 0;
}

int elimRep(int v[], int N)
{
	int dest = 0;
	for (int i = 0; i < N; ++i)
		if (!pertence(v, i, v[i]))
			v[dest++] = v[i];
	return dest;
}

// 34
int elimRepOrd(int v[], int N)
{
	int dest = 0;
	for (int i = 0; i < N; ++i)
		if (i == 0 || v[i] != v[i-1])
			v[dest++] = v[i];
	return dest;
}

// 35
int comunsOrd(int a[], int na, int b[], int nb)
{
	int count = 0, ia = 0, ib = 0;
	while (ia < na && ib < nb)
		if (a[ia] == b[ib])
		{
			++count;
			++ia;
			++ib;
		}
		else
			if (a[ia] < b[ib])
				++ia;
			else
				++ib;
	return count;
}

// 36
int count(int v[], int N, int x)
{
	int count = 0;
	for (int i = 0; i < N; ++i)
		if (x == v[i])
			++count;
	return count;
}

int min(int x, int y)
{
	if (x < y)
		return x;
	else
		return y;
}

int comuns(int a[], int na, int b[], int nb)
{
	int r = 0, ia;
	for (ia = 0; ia < na; ++ia)
	    if (pertence(b, nb, a[ia]))
	        ++r;
	return r;
}

// 37
int minInd(int v[], int n)
{
	int min = 0;
	if (n == 0)
		return -1;
	for (int i = 0; i < n; ++i)
		if (v[i] < v[min])
			min = i;
	return min;
}

// 38
void somasAc(int v[], int Ac[], int N)
{
	Ac[0] = v[0];
	for (int i = 1; i < N; ++i)
		Ac[i] = Ac[i-1] + v[i];
}

// 39
int triSup(int N, float m[N][N])
{
	for (int l = 1; l < N; ++l)
		for (int c = 0; c < l; ++c)
			if (m[l][c] != 0)
				return 0;
	return 1;
}

// 40
void swapInt(float *x, float *y)
{
	float temp;
	temp = *x;
	*x = *y;
	*y = temp;
}

// 41
void transposta(int N, float m[N][N])
{
	for (int l = 0; l < N; ++l)
		for (int c = 0; c < l; ++c)
			swapInt(&m[l][c], &m[c][l]);
}

// 42
void unionSet(int N, int v1[N], int v2[N], int r[N])
{
	for (int i = 0; i < N; ++i)
		r[i] = v1[i] | v2[i];
}

// 43
void intersectSet(int N, int v1[N], int v2[N], int r[N])
{
	for (int i = 0; i < N; ++i)
		r[i] = v1[i] & v2[i];
}

// 44
void intersectMSet(int N, int v1[N], int v2[N], int r[N])
{
	for (int i = 0; i < N; ++i)
		r[i] = min(v1[i], v2[i]);
}

// 45
int max(int x, int y)
{
	if (x > y)
		return x;
	else
		return y;
}

void unionMSet(int N, int v1[N], int v2[N], int r[N])
{
	for (int i = 0; i < N; ++i)
		r[i] = max(v1[i], v2[i]);
}

// 46
int cardinalMSet(int N, int v[N])
{
	int sum = 0;
	for (int i = 0; i < N; ++i)
		sum += v[i];
	return sum;
}


// 47
typedef enum movimento{Norte, Oeste, Sul, Este} Movimento;
typedef struct posicao{int x, y;} Posicao;

Posicao posFinal(Posicao inicial, Movimento mov[], int N)
{
	for (int i = 0; i < N; ++i)
	{
		switch (mov[i])
		{
			case Norte:
				++inicial.y;
				break;
			case Sul:
				--inicial.y;
				break;
			case Este:
				++inicial.x;
				break;
			case Oeste:
				--inicial.x;
				break;
		}
	}
	return inicial;
}

// 48
int caminho(Posicao inicial, Posicao final, Movimento mov[], int N)
{
	int i = 0;
	while (inicial.x != final.x || inicial.y != final.y)
	{
		if (i == N)
			return -1;
		if (inicial.x < final.x)
		{
			mov[i++] = Este;
			++inicial.x;
		}
		else if (inicial.x > final.x)
		{
			mov[i++] = Oeste;
			--inicial.x;
		}	
		else if (inicial.y < final.y)
		{
			mov[i++] = Norte;
			++inicial.y;
		}
		else if (inicial.y > final.y)
		{
			mov[i++] = Sul;
			--inicial.y;
		}
	}
	return i;
}

// 49
int abs(int x)
{
	return x < 0 ? -x : x;
}

int pseudoNorma(Posicao coord)
{
	return abs(coord.x) + abs(coord.y);
}

int maisCentral(Posicao pos[], int N)
{
	int bestIndex = 0;
	for (int i = 1; i < N; ++i)
		if (pseudoNorma(pos[i]) < pseudoNorma(pos[bestIndex]))
			bestIndex = i;
	return bestIndex;
}

// 50
int isVizinho(Posicao p0, Posicao p1)
{
	return (abs(p0.x - p1.x) == 1 && p0.y == p1.y) || (abs(p0.y -  p1.y) == 1 && p0.x == p1.x);
}


int vizinhos(Posicao p, Posicao pos[], int N)
{
	int i, r = 0;
	for (i = 0; i < N; ++i)
		if (isVizinho(p, pos[i]))
			++r;
	return r;
}
